import React, { Component } from 'react';
import { IonItem, IonInput, IonButton, IonLabel} from '@ionic/react';
import { sha256 } from 'js-sha256';
import { LINK_SERVER } from '../../Const';

class SetNewPassword extends Component{
    constructor() {
        super()
        this.state = {
            inputPass: "",
            inputConfirm:"",
            showError:false,
            inputAnswer:""
        }
    }
    _handlePassChange=(e)=>{
        this.setState({inputPass: e.target.value})
        console.log(this.state.inputPass)
    }
    _handleConfirmChange=(e)=>{
        
        this.setState({inputConfirm: e.target.value})
        console.log(this.state.inputConfirm)
        
    }
    _handleSubmit = (e) => {
        e.preventDefault()
        //Verificamos que el correo del usuario exista
        this.setState({muestraError: false})

        if(this.state.inputConfirm === ''){
            this.setState({muestraError: true})
            return
        }

        if(this.state.inputConfirm !== this.state.inputPass)
        { 
            this.setState({muestraError: true})
            return
        }

        var xhttp = new XMLHttpRequest();   
        var url = LINK_SERVER +  "/account/update_pasword_SA.php";
        xhttp.open("POST", url, true);
        xhttp.setRequestHeader("Content-Type", "application/json");
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState === 4 && xhttp.status === 200) {
                console.log(xhttp.responseText);
                /// Condicionante para Login o para rechazar al usuario.
                if(xhttp.responseText === "1")//1: el usuario está registrado. 0: no existe el usuario
                {
                    console.log("Contraseña actualizada")
                    
                }
            }
        }
        
        var hashPass = sha256(this.state.inputPass);
        var hashAnswer = sha256(this.state.inputAnswer);
        var hashMail = sha256();//props?
        var data = JSON.stringify({ "correo": hashMail, "respuesta":hashAnswer, "pass": hashPass });
        xhttp.send(data);

    }

    render(){
        const { showError } = this.state;
        return(
            <form onSubmit={ this._handleSubmit }>
                <IonItem lines="none" className="password-item"></IonItem>
                <IonItem lines="none" className="password-item">
                <IonLabel position= "floating">Escribe tu respuesta</IonLabel>
                    <IonInput 
                        className="password-input"
                        required
                        type="email"
                        placeholder="Contraseña"
                        onIonChange={ this._handlePassChange }>
                    </IonInput>
                </IonItem>
                <IonItem lines="none" className="password-item">
                    { showError ? <label className="recover-error-alert"> 
                    Las contrasñas no coinciden
                    Por favor inténtalo de nuevo, o regístrate con una cuenta nueva
                    </label>
                    : null }
                    <IonLabel position= "floating">Escribe tu contraseña</IonLabel>
                    <IonInput 
                        clearOnEdit="false"
                        className="password-input"
                        required
                        type="email"
                        placeholder="Contraseña"
                        onIonChange={ this._handlePassChange }>
                    </IonInput>
                </IonItem>
                <IonItem lines="none" className="password-item" >
                    <IonLabel position= "floating">Escribe tu contraseña</IonLabel>
                    <IonInput 
                        clearOnEdit="false"
                        className="password-input"
                        required
                        type="email"
                        placeholder="Confirmación de Contraseña"
                        onIonChange={ this._handleConfirmChange }>
                    </IonInput>
                </IonItem>
                
                <IonButton 
                    className="password-button"
                    type="submitButton" 
                    expand="block">  
                    Cambiar contraseña
                </IonButton>

            </form>
        )
    }



}

export default SetNewPassword;