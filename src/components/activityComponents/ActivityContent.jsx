import React, { Component } from 'react';
import ActivityOptions from '../activityComponents/ActivityOptions';
import ActivityLessons from '../../lessons-json/activity-lessons';
import { IonCardContent, IonCardSubtitle } from '@ionic/react';
import './ActivityContent.css';
class ActivityContent extends Component {
    constructor () {
        super () 
        this.state = {
            lessonName: "",
            intructions: "", 
            type: "",
            arraySound: [],
            handleAnswers: undefined,
            handleSong: undefined
        }
    }
    
    componentDidMount(){
        Object.entries(ActivityLessons).map( activity => {
            if ( this.props.lessonName === activity[0]) {
                let arraySound = []
                let type = activity[1]["Tipo"]
                switch (activity[1]["Lesson"]) {
                    case "INTERVALO":
                        let len = activity[1]["Distancia"].length
                        let indexType = this._getRandomInt(0, len)
                        if(len <= 2){
                            type = activity[1]["Tipo"][indexType]
                        }
                        arraySound = this._createActivity(activity, this._arrayInterval, indexType)
                        break;
                    case "ESCALA":
                        arraySound = this._createActivity(activity, this._arrayScales, -1)
                        break;
                    case "ACORDE":
                        arraySound = this._createActivity(activity, this._arrayChord, -1)
                        break;
                    case "ALTURA":
                        arraySound = this._createActivity(activity, this._arrayPitch, -1)
                        break;
                    case "DURACION":
                        arraySound = this._createActivity(activity, this._arrayDuration, -1)
                        break;
                    default:
                        break;
                }
                //
                this.setState({
                    intructions: activity[1]["Instrucciones"],
                    type: type,
                    arraySound: arraySound[0],
                    answer: arraySound[1],
                    handleAnswers: this.props.handleAnswers,
                    handleSong: this.props.handleSong
                })
                console.log(arraySound[1])
            }
            return null
        } )
    }

    _createActivity = (activity, optionFunc, indexType) => {
        let root = this._getRandomInt(48, 59)
        let candidates = [0,1,2,3]
        let array = [[],[],[],[]]

        let pos = this._getRandomInt(0, candidates.length) 
        if(indexType === -1){
            array[candidates[pos]] = optionFunc(root, activity[1]["Distancia"])
        } else {
            array[candidates[pos]] = optionFunc(root, activity[1]["Distancia"][indexType])
        }
        pos = candidates.splice(pos, 1);

        let len = activity[1]["Opc"].length
        for(let i = candidates.length; i > 0; i--) {
            let pos  = this._getRandomInt(0, candidates.length)
            array[candidates[pos]] = optionFunc(root, activity[1]["Opc"][this._getRandomInt(0, len)])
            candidates.splice(pos, 1);
        }
        return [array, pos[0]]
    }

    _arrayDuration = (root, distance) => {
        let sound = []
        for(let i = 0; i < distance; i++){
            sound.push([root])
        }
        sound.push([])
        return sound
    }

    _arrayPitch = (root, distance) => {
        let root_2 = this._getRandomInt(distance[0], distance[1])
        let sound = [
            [root_2],
            [root_2], 
            [root_2],
            [root_2],
            [root_2],
            [root_2], 
            [root_2],
            [root_2],
            [root_2],
            [root_2],
            []
        ]
        return sound
    }

    _arrayInterval = (root, distance) => {
        let sound = [
            [root],
            [root], 
            [root],
            [root], 
            [root + distance],
            [root + distance],
            [root + distance],
            [root + distance],
            [],
            [root, root + distance],
            [root, root + distance], 
            [root, root + distance],
            [root, root + distance],
            [root, root + distance],
            [root, root + distance], 
            [root, root + distance],
            [root, root + distance],
            []
        ]
        return sound
    }

    _arrayChord = (root, distance) => {
        let chord = []
        for(let i = 0; i < 8; i++){
            chord.push(distance.map( x => x + root))
        }
        chord.push([])
        return chord
    }

    _arrayScales (root, distance) {
        let scale = []
        scale.push([root])
        scale.push([root])
        for(let i = 0; i < distance.length; i++){
            scale.push([root + distance[i]])
            scale.push([root + distance[i]])
        }
        for(let i = distance.length - 2; i >= 0; i--){
            scale.push([root + distance[i]])
            scale.push([root + distance[i]])
        }
        scale.push([root])
        scale.push([root])
        scale.push([])
        return scale
    }

    _getRandomInt(min, max) {
        return Math.floor(Math.random() * (max - min)) + min;
    }

    render() {
        const { 
            arraySound, 
            answer, 
            type, 
            intructions, 
            handleAnswers, 
            handleSong } = this.state
        return (
            <div>
                <IonCardSubtitle className="title-content">
                    <p className="activity-title">{ intructions + type }</p>
                </IonCardSubtitle>
                <IonCardContent>
                    <ActivityOptions 
                        arraySound={ arraySound } 
                        answer={ answer } 
                        handleAnswers={ handleAnswers } 
                        handleSong={ handleSong }
                        disabled={ this.props.disabled }/>
                </IonCardContent>
            </div>
        );
    }
}

export default ActivityContent;