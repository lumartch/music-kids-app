import React, { Component } from 'react';
import SoundfontProvider from './SoundfontProvider';
import { Piano } from 'react-piano';
import 'react-piano/dist/styles.css';
import "./BasicPiano.css";

const audioContext = new (window.AudioContext || window.webkitAudioContext)();
const soundfontHostname = 'assets/music_resources/soundfont';

let noteRange = {
    first: 48,
    last: 72,
}

class BasicPiano extends Component {
    constructor(){
        super()
        this.state = {
            activeNotesIndex: 0,
            isPlaying: false,
            stopAllNotes: () => console.warn('stopAllNotes not yet loaded'),
            song: [],
            instrumentName: "",
            isPlayback: false
        };
    }

    componentDidMount(){
        this.setState({
            song: this.props.song, 
            instrumentName: this.props.instrumentName, 
            isPlayback: this.props.isPlayback,
        })
        noteRange = this.props.noteRange
    }

    async componentDidUpdate(prevProps, prevState) {
        if(prevProps.instrumentName !== this.props.instrumentName){
            this.setState({ 
                instrumentName: this.props.instrumentName,
            });
        } 

        if(this.props.isPianoHero === true){
            if(prevProps.song !== this.props.song){
                this.setState({ 
                    song: this.props.song,
                    isPlaying: true,
                });
            }
            if(prevState.isPlaying !== this.state.isPlaying) {
                if (this.state.isPlaying) {
                    this.playbackIntervalFn = setInterval(() => {
                        this.setState({
                            activeNotesIndex: (this.state.activeNotesIndex + 1) % this.state.song.length,
                        });
                        if((this.state.activeNotesIndex + 1) % this.state.song.length === 0){
                            this.setState({
                                isPlaying: false,
                                activeNotesIndex: 0
                            })
                        }
                    }, 200);
                } else {
                    this.setState({
                        activeNotesIndex: 0,
                        song: [[]]
                    });
                }
                clearInterval(this.playbackIntervalFn);
            }
        } else {
            if(prevState.song !== this.props.song){
                this.setState({ 
                    song: this.props.song,
                    isPlaying: true,
                });
            }
            if(prevState.isPlaying !== this.state.isPlaying) {
                if (this.state.isPlaying) {
                    this.playbackIntervalFn = setInterval(() => {
                        this.setState({
                            activeNotesIndex: (this.state.activeNotesIndex + 1) % this.state.song.length,
                        });
                        if((this.state.activeNotesIndex + 1) % this.state.song.length === 0){
                            clearInterval(this.playbackIntervalFn);
                            //his.state.stopAllNotes();
                            this.setState({
                                isPlaying: false,
                                activeNotesIndex: 0
                            })
                        }
                    }, 200);
                } else {
                    clearInterval(this.playbackIntervalFn);
                    //this.state.stopAllNotes();
                    this.setState({
                        activeNotesIndex: 0,
                        song: [[]]
                    });
                }
            } 
        }
    }

    _setPlaying = (value, song) => {
        this.setState({ 
            isPlaying: value, 
            song: song
        });
    }

    _renderPianoPlayBack = (instrumentName) => {
        return (
            <div>
                <SoundfontProvider
                    instrumentName = { instrumentName }
                    audioContext = { audioContext }
                    hostname = { soundfontHostname }
                    onLoad={({ stopAllNotes }) => this.setState({ stopAllNotes })}
                    render = { ({ isLoading, playNote, stopNote, stopAllNotes }) => (
                        <Piano 
                            noteRange = { noteRange }
                            playNote = { playNote }
                            stopNote = { stopNote }
                            disabled ={ isLoading || !this.state.isPlaying }
                            activeNotes={
                                this.state.isPlaying ? this.state.song[this.state.activeNotesIndex] : []
                            }
                        />
                    )}
                />
            </div>
        )
    }

    _renderBasicPiano = (instrumentName) => {
        return(
            <SoundfontProvider
                instrumentName={ instrumentName }
                audioContext={ audioContext }
                hostname={ soundfontHostname }
                render={ ({ isLoading, playNote, stopNote, stopAllNotes}) => (
                    <Piano 
                        noteRange={ noteRange }
                        playNote={ playNote }
                        stopNote={ stopNote }
                        disabled={ isLoading }
                        stopAllNotes={ stopAllNotes }
                    />
                )}
            />
        )
    }

    render() {
        const { instrumentName, isPlayback } = this.state
        return (
            <div>
                {
                    isPlayback
                    ? this._renderPianoPlayBack(instrumentName)
                    : this._renderBasicPiano(instrumentName)
                }
            </div>
        );
    }
}

BasicPiano.defaultProps = {
    noteRange : {
        first: 48,
        last: 72,
    }
}

export default BasicPiano;