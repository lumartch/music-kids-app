import React, { Component } from 'react';
import { IonItem, IonCardTitle, IonIcon, IonCardContent, IonCard, IonLabel, IonButton, IonSelect, IonSelectOption } from '@ionic/react';
import { heartOutline, heart, trophyOutline, trophy, eye, play } from "ionicons/icons";
import Notes from '../../images/littleStarSong.png'
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import store from '../../redux/store';
import { updateFavoriteSong } from '../../redux/actions/SongsActions';
import "./Song.css";
import {fetchFavorite} from './SongFavorite';
//import { IS_LOCAL, LINK_SERVER } from '../../Const';

class Song extends Component {
    constructor(){
        super()
        this.state = {
            songId: undefined,
            show: false,
            title: undefined,
            author: undefined,
            duration: undefined,
            soundfont: undefined,
            path: undefined,
            rating: undefined, 
            views: undefined
        }
    }

    componentDidMount(){
        this.setState({
            songId: this.props.id,
            title: this.props.title,
            author: this.props.author,
            duration: this.props.duration,
            soundfont: "acoustic_grand_piano",
            path: this.props.path,
            icon: this.props.favorite === 0 ? heartOutline : heart, 
            rating: this.props.rating,
            finishedIcon: this.props.finished === 0 ? trophyOutline : trophy,
        })
    }

    _changeSoundfont = (e) => {
        this.setState( {
            soundfont: e.detail.value
        } )
    }
    
    _changeFavorite = async () => {
        let response = await fetchFavorite(this.props.user_state.user["correo"],  this.state.songId, this.state.icon === heart ? 0 : 1)
        if (response === "1") {
            if(this.state.icon === heartOutline){
                this.setState({ icon: heart})
                store.dispatch(updateFavoriteSong(this.state.songId, 1))
            } else {
                this.setState({ icon: heartOutline})
                store.dispatch(updateFavoriteSong(this.state.songId, 0))
            }
        }
    }
    
    render() {
        const { title, author, duration, soundfont, icon, show,  } = this.state
        return (
            <IonCard className="song-container">
                <IonItem button className="song-description ion-no-padding">
                    <IonCardTitle className="song-title" onClick={() => this.setState({show: !this.state.show})}>
                        {title} 
                    </IonCardTitle>
                    <IonItem button className="song-buttons" lines="none">
                        <IonIcon onClick={ this._changeFavorite } className="icon-finished" icon={ this.props.finished === 0 ? trophyOutline : trophy }/>
                        <IonIcon onClick={ this._changeFavorite } className="icon-favorite" icon={ icon }/>
                    </IonItem>
                </IonItem>
                {
                    show
                    ? <IonCardContent>
                        <div className="song-div-container"> 
                            <img alt={Notes} className="song-img" src={Notes}></img>
                        </div>
                        <div className="song-div-container song-div-info"> 
                            <IonLabel> { author } </IonLabel><br/>
                            <IonLabel>{ duration }</IonLabel><br/>
                            <IonLabel> <IonIcon icon={eye}></IonIcon> { this.props.views }</IonLabel>
                            
                        </div>
                        <div className="div-buttons"> 
                            <IonSelect classname="soundfont" value={ soundfont } onIonChange={ this._changeSoundfont }>
                                <IonSelectOption value="acoustic_grand_piano">Piano</IonSelectOption>
                                <IonSelectOption value="acoustic_guitar_steel">Guitarra</IonSelectOption>
                                <IonSelectOption value="flute">Flauta</IonSelectOption>
                                <IonSelectOption value="violin">Violin</IonSelectOption>
                                <IonSelectOption value="church_organ">Orgáno</IonSelectOption>
                                <IonSelectOption value="accordion">Acordión</IonSelectOption>
                                <IonSelectOption value="acoustic_bass">Bajo</IonSelectOption>
                                <IonSelectOption value="alto_sax">Saxofón</IonSelectOption>
                                <IonSelectOption value="koto">Koto</IonSelectOption>
                            </IonSelect>
                            <Link 
                                to={{
                                    pathname:`/game`,
                                    state: {
                                        path: this.state.path, 
                                        soundfont: this.state.soundfont,
                                        songName: this.state.title,
                                        songId: this.state.songId
                                    }
                                }}
                                className="link-play"
                            >
                                <IonButton className='play'><IonIcon icon={play}></IonIcon></IonButton>
                            </Link>
                        </div>
                    </IonCardContent>
                    : null
                }
            </IonCard>
        );
    }
}

Song.defaultProps = {
    icon: 0, 
    views: "N/A",
    rating: 2.5
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
}

export default connect(mapStateToProps)(Song);