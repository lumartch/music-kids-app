import React, { Component } from 'react';

class RectangleRoll extends Component {
    constructor(){
        super()
        this.state = {
            interval: undefined, 
            bottom: 0,
            visible: true, 
            type: undefined, 
            left: undefined,
            width: undefined, 
            semiquavers: undefined,
            isActive: false, 
        }
    }
    

    async componentDidMount() {
        let type = undefined
        let left = undefined
        let width = undefined
        let index = undefined
        let heightType = undefined
        const { indexNote, octave, semiquavers } = this.props
        if( indexNote === 0 || indexNote === 2 || indexNote === 4 || indexNote === 5 ||indexNote === 7 ||indexNote === 9 || indexNote === 11 ){
            type = "white"
            width = "6.66667"
            if( indexNote === 0 ||  indexNote === 2 || indexNote === 4 ){
                index = indexNote / 2
            } else {
                index = ( ( indexNote - 5 ) / 2 ) + 3
            }
            left = (6.66667 * 7 * octave) + (index * 6.66667) + "%"
        }
        if( indexNote === 1 || indexNote === 3 || indexNote === 6 || indexNote === 8 || indexNote === 10 ){
            type = "black"
            width = "5"
            if(indexNote === 1 ||  indexNote === 3) { // [0,1]
                //8.333333 left para grupo de dos
                index = ((indexNote - 1) / 2)
                left = octave === 0 ? (3.66667 + (index * 8.333333) + "%") : (50.33333 + (index * 8.333333) + "%")
            } else {
                //7.888888 left para grupo de tres
                index = ( ( indexNote - 6 ) / 2 )
                left = octave === 0 ? (23.3333 + (index * 7.888888) + "%") : (70 + (index * 7.888888) + "%")
            }
        }

        if(window.innerHeight > window.innerWidth){
            heightType = "vw"
        } else {
            heightType = "vh"
        }

        this.setState({ 
            interval: setInterval(this._timer, this.props.intervalTime * 1000), 
            type: type, 
            left: left,
            width: width,
            semiquavers: (semiquavers * 4).toString() + heightType
        });
    }

    componentWillUnmount() {
        this.setState({visible: false})
        clearInterval(this._timer)
    }
    
    _timer = () => {
        let windowSize = 0
        let heightType = undefined
        if(window.innerHeight > window.innerWidth){
            windowSize = window.innerWidth
            heightType = "vw"
        } else {
            windowSize = window.innerHeight
            heightType = "vh"
        }

        if(this.state.bottom < -(windowSize * 0.46) && this.state.isActive === false){
            this.props.noteKey({
                time: this.props.k, 
                flag: "start"
            })
            this.setState({ 
                isActive: true, 
            })
        }

        if(this.state.bottom <= -( (windowSize * 0.46) + ((windowSize * 0.08) * (this.props.semiquavers/2)) ) ) {
            this.props.noteKey({
                time: this.props.k, 
                flag: "end"})
            this.setState({ visible: false, isActive: false })
            clearInterval( this.state.interval )
        } 


        this.setState({ 
            bottom: this.state.bottom - (windowSize * 0.08), 
            semiquavers: (this.props.semiquavers * 4).toString() + heightType
        });
    }

    render() {
        const style = {
            div: {
                left: this.state.left,
                background: this.state.type,
                height: this.state.semiquavers, //Definidio por la duracion
                position: "absolute",
                borderRadius: "5px",
                bottom: this.state.bottom + "px",
                width: this.state.width + "%",
                zIndex: this.props.type === "black" ? 4 : 3,
                borderColor: "lightgrey", 
                borderWidth: "1px", 
                borderStyle: "solid"
            }
        }
        
        return (
            <div style={{ position: "relative", zIndex: 2}}>
                {
                    this.state.visible && !this.props.disabled
                    ? <div style={style.div}> </div>
                    : null
                }
            </div>
        )
    }
}

RectangleRoll.defaultProps = {
    disabled: false
}

export default RectangleRoll;