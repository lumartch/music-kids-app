import React, { Component } from 'react';
import "./PianoKey.css";

class PianoKey extends Component {
    constructor(){
        super()
        this.state = {
            style: undefined, 
            keyType: "", 
        }
    }

    componentDidMount(){
        this.setState({
            style: this._generateKey()
        })
    }

    _generateKey(){
        let style = {
            left: undefined,
            width: undefined, 
            height: undefined, 
            zIndex: undefined, 
            position: "absolute",
            padding: 0,
            borderColor: "lightgrey", 
            borderWidth: "1px", 
            borderStyle: "solid", 
            borderBottomRightRadius: "10px",
            borderBottomLeftRadius: "10px"
        }
        let index = undefined
        const { indexNote, octave } = this.props
        if( indexNote === 0 || indexNote === 2 || indexNote === 4 || indexNote === 5 ||indexNote === 7 ||indexNote === 9 || indexNote === 11 ){
            style.background = "white"
            style.width = "6.66667%"
            if( indexNote === 0 ||  indexNote === 2 || indexNote === 4 ){
                index = indexNote / 2
            } else {
                index = ( ( indexNote - 5 ) / 2 ) + 3
            }
            style.left = (6.66667 * 7 * octave) + (index * 6.66667) + "%"
            style.height = "100%";
            style.zIndex = "3"
        }

        if( indexNote === 1 || indexNote === 3 || indexNote === 6 || indexNote === 8 || indexNote === 10 ){
            style.background = "black"
            style.width = "5%"
            if(indexNote === 1 ||  indexNote === 3) { // [0,1]
                //8.333333 left para grupo de dos
                index = ( (indexNote - 1) / 2 )
                style.left = octave === 0 ? (3.66667 + (index * 8.333333) + "%") : (50.33333 + (index * 8.333333) + "%")
            } else {
                //7.888888 left para grupo de tres
                index = ( ( indexNote - 6 ) / 2 )
                style.left = octave === 0 ? (23.3333 + (index * 7.888888) + "%") : (70 + (index * 7.888888) + "%")
            }
            style.height = "60%";
            style.zIndex = "4"
        }
        this.setState( { keyType: "piano-key-" + style.background } )
        return style
    }

    render() {
        const { style, keyType } = this.state
        return (
            <button disabled={ this.props.disabled }
                className={keyType}
                style={ style }
            />
        );
    }
}

export default PianoKey;