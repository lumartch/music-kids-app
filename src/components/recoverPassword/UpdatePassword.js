import { LINK_SERVER, IS_LOCAL } from '../../Const';

export const updatePasswordFetch = (email, answer, password) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();
        let url =  IS_LOCAL ? LINK_SERVER + "/account/" : LINK_SERVER + "/account/setPassword.php";
        xhttp.open(IS_LOCAL ? "PUT" : "POST", url , true);
        xhttp.onload = () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                resolve(xhttp.responseText)
            } else {
                resolve({ 
                    showAlert: true
                });
            }
        };
        var data = JSON.stringify({ 
            "correo": email, 
            "respuesta": answer, 
            "pass": password 
        });
        xhttp.send(data);
    })
}