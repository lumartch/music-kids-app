import { LINK_SERVER, IS_LOCAL } from '../../Const';

export const FetchNewAccount = (email, password, nickname, name, phone, sq, sa, avatar) => {
    return new Promise((resolve, reject) => {
        var xhttp = new XMLHttpRequest();   
        var url = IS_LOCAL ? LINK_SERVER + "/account/" : LINK_SERVER +  "/account/setNewUser.php";
        xhttp.open("POST", url, true);
        xhttp.onload = () => {
            if (xhttp.status >= 200 && xhttp.status < 300) {
                if(xhttp.responseText === "1") {
                    let json = JSON.stringify({
                        "avatar": avatar,
                        "correo": email, 
                        "nickname": nickname, 
                        "name": name,
                        "phone": phone
                    })
                    resolve(JSON.parse(json))
                } else {
                    resolve({ state: false })
                }
            } else {
                resolve({ state: false });
            }
        };

        var data = JSON.stringify({ 
            "correo": email, 
            "pass": password, 
            "nickname": nickname, 
            "nombre": name,
            "telefono": phone,
            "pregunta": sq,
            "respuesta": sa,
            "avatar": avatar
        });
        xhttp.send(data);
    })
    
}