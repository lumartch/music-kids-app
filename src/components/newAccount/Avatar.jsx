import React, {Component} from 'react';
import './Avatar.css';

class Avatar extends Component {
    constructor() {
        super();
        this.state = {  
            nameImage: ""
        };
    }

    componentDidMount = () => {
        this.setState ({ nameImage: this.props.src });
    }

    render(){
        return(
            <div className="container">
                <img alt= { this.props.src } className="item-avatar" src= { this.props.src } />
            </div>
        )
    }
} 
export default Avatar;