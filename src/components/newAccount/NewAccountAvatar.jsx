import React, {Component} from 'react';
import { IonItem, IonCard } from '@ionic/react'
import AvatarList from '../../Avatar-json/avatars.json';
import AvatarCard from './AvatarCard';
import Avatar from './Avatar.jsx';
import './NewAccountAvatar.css';

class NewAccountAvatar extends Component{
    constructor() {
        super()
        this.state = {
            src: undefined,
            flag: false
        }
    }

    _handleChangeSrc = (value, name) => {
        this.setState( { src: value, flag: !this.state.flag } )
        this.props.getAvatar(name)
    }

    _handleClickDefault = () => {
        this.setState( { flag: !this.state.flag } )
    }

    _renderAvatars = () => {
        return Object.entries(AvatarList).map( (animal, key) => {
            return <AvatarCard
                    key={ key }
                    color={ animal[1]["RGB"] }
                    name={ animal[0] }
                    handleChangeSrc={ this._handleChangeSrc }
                    path={ animal[1]["path"] }
                    size={ animal[1]["tamanio"] }
                    favColor={ animal[1]["color favorito"] } 
                    description={ animal[1]["descripcion"] }
                    hobby={ animal[1]["pasatiempo"] }
                    loves={ animal[1]["gustos"] }
                    hates={ animal[1]["odia"] }
                    />
        })
    }

    render(){
        const { flag, src } = this.state;
        return (
            <IonCard className="newAccount-card" disabled={this.props.disabled}>
                <IonItem lines="none" className="back-ground" button onClick = { this._handleClickDefault }>
                    {
                        src === undefined
                        ? <Avatar src = { this.props.myAvatar } />
                        : <Avatar src = { this.props.myAvatar } />
                    }
                </IonItem>
                {
                    flag
                    ? !this.props.disabled ? this._renderAvatars() : null
                    : null
                }
            </IonCard>   
        )
    }
}

NewAccountAvatar.defaultProps = {
    myAvatar: "assets/avatars/cat.jpg",
    disabled: false
}

export default NewAccountAvatar;
