import React, { Component } from 'react';
import { IonPage,
    IonItem, 
    IonToolbar, 
    IonButtons, 
    IonMenuButton, 
    IonTitle, 
    IonContent,
    IonCard, 
    IonCardContent, 
    IonIcon, IonInput, IonButton, IonAlert } from '@ionic/react';
import { save, pencil, closeCircle } from 'ionicons/icons';
import NewAccountAvatar from '../../components/newAccount/NewAccountAvatar';
import { connect } from 'react-redux';
import { Redirect } from 'react-router';
import { updateData } from '../../redux/actions/LoginActions';
import updateImg from '../../images/updateData.png';
import store from '../../redux/store';
import { setSettingsFetch } from './SettingsConst';
import "./SettingsPage.css";

class SettingsPage extends Component {
    constructor(){
        super()
        this.state = {
            avatarId: undefined,
            avatarPath: undefined,
            name: undefined,
            nickname: undefined,
            phone: undefined,
            password: "",
            confirmPassword: "",
            updateInfo: undefined,
            email: undefined,
            disabled: true,
            changePass: undefined,
            showAlert: false,
            alertMessage: undefined
        }
    }

     async componentDidMount(){
        if(!this.props.user_state.login){
            return 
        }
        this.setState({ 
            avatarPath: "assets/avatars/" + this.props.user_state.user.avatar + ".jpg",
            avatarId: this.props.user_state.user.avatar,
            name: this.props.user_state.user.name,
            nickname: this.props.user_state.user.nickname,
            phone: this.props.user_state.user.phone,
            email: this.props.user_state.user.correo,
            updateInfo: true,
            icons: pencil,
            changePass: false
        })
    }

    _generateHTML = (myText, myImage) =>{
        return (`<div> 
                    <label class="alert-label-settings-update"> ${myText} </label> 
                    <img class="alert-img-settings" alt="${myImage}" src=${myImage} /> 
                </div>`)
    }

    _handleSubmit = async(e) => {
        e.preventDefault()
        const { phone, password, confirmPassword, avatarId, nickname, name } = this.state
        let invalidName = name.replace(/\s/g, '').length === 0
        let invalidNickname = nickname.replace(/\s/g, '').length === 0
        let invalidPass = password.replace(/\s/g, '').length === password.length
        if(password === confirmPassword
            && this._isPhoneCorrect(phone) && !invalidName && !invalidNickname && invalidPass){
                let resolve = await setSettingsFetch(this.props.user_state.user.correo, password, nickname, name, phone, avatarId);
                if(resolve !== false){
                    store.dispatch(updateData(resolve))
                    this.setState({ 
                        disabled: true, 
                        icons: pencil,
                        changePass: false,
                        password: "",
                        confirmPassword: "",
                        showAlert: true,
                        alertMessage: this._generateHTML("¡Los datos han sido actualizados correctamente!", updateImg)
                    })
                } else {
                    this.setState({
                        disabled: true, 
                        icons: pencil,
                        password: "",
                        confirmPassword: "" ,
                        showAlert: true,
                        alertMessage: "En este momento no contamos con internet para actualizar sus ajustes, intentelo mas tarde. :c"
                    })
                }
        } else if(password !== confirmPassword){
            this.setState({
                showAlert: true,
                alertMessage: '<label class="alert-label-settings">Las contraseñas no coinciden, reviselas por favor. :3 </label>'
            })
        }  else if (!this._isPhoneCorrect(phone)) {
            this.setState({
                showAlert: true,
                alertMessage: '<label class="alert-label-settings"> Favor de ingresar un teléfono válido. :3 </label>'
            })
        } else if(invalidName || invalidNickname || !invalidPass){
            this.setState({
                showAlert: true,
                alertMessage: '<label class="alert-label-settings"> No se pueden ingresar solo espacios en los campos.</label>'
            })
        }
    }

    _handleDisabled = (e) => {
        e.preventDefault()
        this.setState({ disabled: false, icons: save})
    }

    _handleChangeName = (e) => {
        this.setState({ name: e.target.value })
    }
    
    _handleChangePhone = (e) => {
        this.setState({ phone: e.target.value })
    }

    _isPhoneCorrect = (phone) => {
        let rx = new RegExp("\\d{10}$");
        if (rx.test(phone)){
            return true
        } else {
            return false
        }
    }

    _handleChangeNickname = (e) => {
        this.setState({ nickname: e.target.value })
    }

    _handleChangePassword = (e) =>{
        this.setState({ password: e.target.value })
    }
    
    _handleChangeConfirmPassword = (e) =>{
        this.setState({ confirmPassword: e.target.value })
    }

    componentDidUpdate(prevProps, prevState){
        if(prevState.password !== this.state.password && !this.state.changePass) {
            this.setState({ changePass: true })
        }
        if(this.state.password === "" && this.state.changePass) {
            this.setState({ changePass: false, confirmPassword: "" })
        }

        if(prevProps.history.location.pathname !== '/settings' && !this.state.disabled){
            this._handleCancel()
        }
    }

    _setAvatar = (avatar) => {
        this.setState({ avatarPath: "assets/avatars/" + avatar + ".jpg", avatarId: avatar })
    }

    _handleCancel = () => {
        this.setState({ 
            avatarPath: "assets/avatars/" + this.props.user_state.user.avatar + ".jpg",
            avatarId: this.props.user_state.user.avatar,
            name: this.props.user_state.user.name,
            nickname: this.props.user_state.user.nickname,
            phone: this.props.user_state.user.phone,
            email: this.props.user_state.user.correo,
            updateInfo: true,
            icons: pencil,
            changePass: false,
            disabled: true,
            password: "",
            confirmPassword: ""
        })
    }

    render() {
        const { name, nickname, phone, password, disabled, email, icons, changePass, avatarPath, confirmPassword, showAlert, alertMessage } = this.state
        return (
            !this.props.user_state.login ? <Redirect to="/login"/> : 
            <IonPage>
                <IonAlert
                    isOpen={ showAlert }
                    onDidDismiss={ () => this.setState({ showAlert: false }) }
                    backdropDismiss = { false }
                    cssClass = 'alert-settings'
                    header = { "" }
                    subHeader = { "" }
                    message = { alertMessage }
                    buttons = { ["Ok"] }/>
                <IonContent className="settings-background">
                    <IonToolbar className="settings-toolbar">
                        <IonButtons>
                            <IonTitle color="white"className="settings-title">AJUSTES</IonTitle>
                            <IonMenuButton color="white" autoHide={ false } className="menu-button-settings"/>
                        </IonButtons>
                    </IonToolbar>
                    <form onSubmit={ disabled ?  this._handleDisabled : this._handleSubmit } >
                        <IonCard className="settings-card">
                            <IonCardContent className="settins-card-content">
                                <IonButton type="submit"  className="save-button">
                                    <IonIcon icon={icons} className="save-icon"/>
                                </IonButton>
                                <IonButton className="close-button" onClick={ this._handleCancel } hidden={disabled}>
                                    <IonIcon icon={closeCircle} className="save-icon"/>
                                </IonButton>
                                <NewAccountAvatar 
                                    disabled={disabled} 
                                    getAvatar={ this._setAvatar } 
                                    myAvatar={ avatarPath }/>
                                { disabled ? null : <div className="avatar-settings"><label> Elige tu avatar</label></div>}
                                <label className="title-data">Datos Personales</label>
                                <IonItem className="item-setting" disabled={disabled}>
                                    <div className="settings-info-container"> 
                                        <IonInput 
                                            className="settings-input" 
                                            placeholder="Nombre..." 
                                            value={ name } 
                                            onIonChange={ this._handleChangeName } 
                                            required/>
                                        <IonInput 
                                            className="settings-input" 
                                            placeholder="Apodo..."  
                                            value={ nickname } 
                                            onIonChange={ this._handleChangeNickname } 
                                            required/>
                                        <IonInput 
                                            className="settings-input" 
                                            placeholder="Teléfono..." 
                                            value={ phone } 
                                            onIonChange={ this._handleChangePhone } 
                                            required/>  
                                    </div>
                                </IonItem>
                                <label className="title-data">Seguridad</label>
                                <IonItem className="item-setting" disabled={disabled}>
                                    <div className="settings-info-container"> 
                                        <IonItem className="settins-email-label" lines="none">{email} </IonItem>
                                        <IonInput type="password" 
                                            clearOnEdit="false" 
                                            className="settings-input" 
                                            onIonChange={ this._handleChangePassword } 
                                            placeholder="Actualizar contraseña" 
                                            value={password} required={changePass}/>
                                        {
                                            changePass
                                            ? <IonInput 
                                                type="password" 
                                                clearOnEdit="false" 
                                                className="settings-input" 
                                                onIonChange={ this._handleChangeConfirmPassword} 
                                                placeholder="Confirma tu contraseña" 
                                                value={confirmPassword} required/>
                                            : null
                                        }
                                    </div>
                                </IonItem>
                            </IonCardContent>
                        </IonCard>
                    </form>
                </IonContent>
            </IonPage>
        );
    }
}

const mapStateToProps = state => {
    return {
        user_state: state.user_state
    }
}

export default connect(mapStateToProps)(SettingsPage);