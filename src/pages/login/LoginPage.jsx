import React, { Component } from 'react';
import {IonPage,  IonContent, IonLabel } from '@ionic/react';
import HeaderComponent from '../../components/pageComponents/HeaderComponent'
import Login from '../../components/login/Login';
import { Link, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import './LoginPage.css';

class LoginPage extends Component {
  componentDidMount() {
    if(this.props.user_state.login){
      return 
    }
  }
  
  render() {
    return (
      this.props.user_state.login ? <Redirect to="/home"/> : 
      <IonPage>
        <IonContent className="login-background"> 
          <HeaderComponent headerTitle="Iniciar Sesión" image={'assets/avatars/login.png'} message="¡Hola! Inicia sesión para jugar" />
            <Login></Login>
            <div style={{display: "inline-block", width: "50%"}}>
                <Link to={{pathname: "/recover"}} style={{ textDecoration: 'none' }}>
                  <div style={{textAlign: "center", display: "block" , position:"relative", top:"0px"}}>
                    <img alt='assets/avatars/recover.png' src = {'assets/avatars/recover.png'} className="footer-image-login"/>
                    <IonLabel color="white" style={{ fontFamily: "'Kalam', cursive"}}>¿Olvidaste tu contraseña?</IonLabel>
                  </div>
                </Link>
            </div>

            <div style={{display: "inline-block", width: "50%"}}>
              <Link to={{pathname: "/new"}} style={{ textDecoration: 'none' }}>
                <div style={{textAlign: "center", display: "block", position:"relative", top:"0px"}}>
                  <img alt='assets/avatars/register.png' src = {'assets/avatars/register.png'} className="footer-image-login"/>
                  <IonLabel color="white" style={{ fontFamily: "'Kalam', cursive"}}>Registrate ¡Es muy fácil!</IonLabel>
                </div>
              </Link>
            </div>
        </IonContent>
      </IonPage>
    );
  }
}

const mapStateToProps = state => {
  return {
    user_state: state.user_state
  }
}

export default connect(mapStateToProps)(LoginPage);
