export const LOAD_LESSONS = 'LOAD_LESSONS';
export const UPDATE_LESSON_FIELD = 'UPDATE_LESSON_FIELD'
export const CLEAR_LESSONS = 'CLEAR_SONGS';

export const update = (field, theme, finished) => {
    return {
        type: UPDATE_LESSON_FIELD,
        field: field,
        theme: theme,
        finished: finished
    }
}

export const clear = () => {
    return {
        type: CLEAR_LESSONS
    }
}

export const load = (data) => {
    return {
        type: LOAD_LESSONS,
        data: data
    }
}


export const updateLessonField = (field, theme, finished) => {
    return (dispatch) => {
        dispatch(update(field, theme, finished))
    }
}

export const loadLessons = (json) => {
    return (dispatch) => {
        dispatch(load(json))
    }
}

export const clearLessons = () => {
    return (dispatch) => {
        dispatch(clear())
    }
}