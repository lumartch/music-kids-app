import { SAVE_DATA_SUCCESS, LOGOUT } from "../actions/LoginActions"

const default_user_state = {
    login: false,
    user: { }
}

const user_state = (state = default_user_state, action) => {
    switch (action.type){
        case SAVE_DATA_SUCCESS:
            return {
                login: true,
                user: action.data
            }
        case LOGOUT: 
            return {
                login: false,
                user: {}
            }
        default:
            return state
    }
}

export default user_state;