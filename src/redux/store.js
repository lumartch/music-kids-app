import rootReducers from "./reducers/rootReducers";
import { createStore, applyMiddleware } from "redux";
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

function saveToLocalStorage (state) {
    try {
        const serializedState = JSON.stringify(state)
        localStorage.setItem('state', serializedState)
    }catch(e) {
        console.log(e)
    }
}

function loadFromlocalStorage() {
    try {
        const serializedState = localStorage.getItem('state')
        if(serializedState === null) {
            return undefined
        }
        return JSON.parse(serializedState)
    } catch (e) {
        console.log(e)
        return undefined
    }
}

const persistedState = loadFromlocalStorage(); 

const store = createStore(rootReducers, persistedState, composeWithDevTools(
    applyMiddleware(thunk)
))

store.subscribe( () => saveToLocalStorage(store.getState()))

export default store;